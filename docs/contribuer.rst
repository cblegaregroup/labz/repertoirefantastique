.. _contribuer:

########################
Contribuer au Répertoire
########################


Version facile, en ligne dans GitLab
====================================

Rendez-vous sur la `page du projet du Répertoire fantastique`_ sur GitLab
et cliquez le bouton **Web IDE**.

.. _page du projet du Répertoire fantastique: https://gitlab.com/cblegaregroup/labz/repertoirefantastique

Faites vos changements puis cliquez **Create commit**.

Laissez **Create a new branch** et, optionnellement donnez un nom à cette
branche en plus d'un message de commit plus évocateur. Cliquez **Commit**.

Pour une nouvelle branche, complétez la création de la *merge request* (*MR*)
avec le bouton bleu **Create merge request** en bas.  De l'écran d'une
merge request, vous pouvez toujours accéder au **Web IDE** avec le bouton
**Code** et au site composé de vos contribution avec le bouton **View latest app**.

Quand vous avez fini, faites signe pour qu'on fusionne sur la branche principale.


.. _contribuer-terminal:

Version simple, au terminal
===========================

Ouvrez un terminal et entrez les commandes

.. code-block:: shell

    python --version

.. code-block:: shell

    git --version

Si le résultat ressemble à un numéro de version, bravo! et passez à l'étape
suivante.

Sinon, assurez-vous d'avoir bien installé Git_ et Python_ ou recommencez.

.. _Git: https://git-scm.com/
.. _Python: https://www.python.org/

Récupérez le code sur votre ordinateur et placez-vous sur une branche

.. code-block:: shell

    git clone git@gitlab.com:cblegaregroup/labz/repertoirefantastique.git
    cd repertoirefantastique
    git checkout -b objet-de-ma-contribution

Installez Nox_, prenez connaissance des sessions existantes et vous êtes prêt.

.. code-block:: shell

    pip install nox
    nox --list-sessions

.. _Nox: https://nox.thea.codes/en/stable/index.html

Modifiez les fichiers sans crainte, inspectez les fichiers obtenus avec
``nox``, et envoyez régulièrement votre contenu sur le serveur:

.. code-block:: shell

    git add -A
    git commit -m "Court résumé de ma sauvegarde"
    git push

Lisez attentivement les messages que ``git`` vous renvoie, ils sont souvent
utiles.  Par exemple, si ``git`` vous renvoie un message du serveur distant
vous suggérant de créer une *merge request* (*MR*), considérez obtempérer
et cliquer sur le lien.


Version populaire, avec VS Code
===============================

Installez `Visual Studio Code`_ (*VSCode*) et ouvrez-le.  Suivez les
instruction pour ouvrir un répertoire d'un dépôt cloné (**Cloner un dépôt**)
en utilisant l'URL Git appropriée, par exemple

.. code-block:: text

    https://gitlab.com/cblegaregroup/labz/repertoirefantastique.git

Si vous n'arrivez pas à faire faire ce que vous voulez à VSCode, ouvrez
son terminal (menu **Affichage** puis **Terminal**) et essayez
:ref:`contribuer-terminal`.

.. _Visual Studio Code: https://code.visualstudio.com/


Version charmante, avec PyCharm
===============================

Installez PyCharm_ et ouvrez-le.  Suivez les instructions pour ouvrir un
projet d'un gestionnaire de source (**Get from VCS**) en utilisant l'URL
Git appropriée, par exemple

.. code-block:: text

    https://gitlab.com/cblegaregroup/labz/repertoirefantastique.git

Si vous n'arrivez pas à faire faire ce que vous voulez à PyCharm, ouvrez
son terminal (onglet **Terminal** en bas) et essayez :ref:`contribuer-terminal`.

.. _PyCharm: https://www.jetbrains.com/pycharm/


Version procrastination, créer un ticket
========================================

Un peu perdu? Besoin d'information? Créez un ticket dan le `système de suivi`_.

.. button-link:: https://gitlab.com/cblegaregroup/labz/repertoirefantastique/-/issues/new
    :color: primary
    :shadow:

    Créer un ticket

Ne vous souciez que des champs **Title** et **Description**.

.. _système de suivi: https://gitlab.com/cblegaregroup/labz/repertoirefantastique/-/issues
