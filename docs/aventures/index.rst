.. _aventures:

#############
Nos aventures
#############

Explorez les dédales de nos contenus non triés.

.. toctree::
    :maxdepth: 2
    :hidden:

    row/index
    mm/index
    rotrl/index


.. grid:: 2
    :margin: 4 4 0 0
    :gutter: 5


    .. grid-item-card:: La Tyrannie hivernale avec <codename>
        :link: row/index
        :link-type: doc
        :img-bottom: ../assets/CommunityUsePackageLogos-HistoricLogos/Pathfinder/Pathfinder\ Adventure\ Path/12\ Reign\ of\ Winter.png


    .. grid-item-card:: Le Masque de la momie avec <codename>
        :link: mm/index
        :link-type: doc
        :img-bottom: ../assets/CommunityUsePackageLogos-HistoricLogos/Pathfinder/Pathfinder\ Adventure\ Path/14\ Mummys\ Mask.png


    .. grid-item-card:: L'Ascension des seigneurs runiques avec <codename>
        :link: rotrl/index
        :link-type: doc
        :img-bottom: ../assets/CommunityUsePackageLogos-HistoricLogos/Pathfinder/Pathfinder\ Adventure\ Path/1\ Rise\ of\ the\ Runelords.png





