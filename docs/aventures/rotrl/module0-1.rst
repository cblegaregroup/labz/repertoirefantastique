.. _player-guide:

###############
Guide du joueur
###############


Introduction
============

“The town of Sandpoint needs you!”

Bienvenue sur le chemin de l'aventure Rise of the Runelords et bienvenue
à Varisia!
Dans `Pathfinder Adventure Path: Rise of the Runelords Anniversary Edition`,
votre personnage prendra les armes contre le retour d'un mal vieux
de plusieurs siècles cherchant à reconquérir la terre. Avant de vous lancer
dans votre aventure, vous devez savoir certaines choses qui vous aideront
dans votre voyage. Dans ce guide, vous trouverez des conseils utiles sur
la conception de votre personnage, une sélection de traits pour aider votre
personnage à se sentir comme une partie du monde et les événements de la
campagne, et un aperçu des terres que votre personnage visitera. Combattez
fort et gardez la tête froide, car votre personnage est peut-être la seule
chose qui protège Varisia, et même Golarion, de la ruine!

On aime le NPC :npc:`Aldern Goupil`.


Astuces pour les personnages
============================

Une menace massive émerge en Varisie et rassemble ses forces. Seul un groupe
d'aventuriers méfiants peut mettre fin à ce mal, et ils feraient mieux de
se préparer à ce qu'ils affrontent. Les conseils suivants peuvent vous aider
lors de la conception de personnages pour Rise of the Runelords.

Cultures perdues antiques
    Étant donné que le parcours d'aventure Rise of the Runelords révèle une
    ancienne menace réapparaissant dans le monde, les personnages compétents
    en Connaissance (histoire) et Connaissance (arcanes) sont les mieux placés
    pour assembler les pièces. L'accès à une magie puissante à des niveaux
    supérieurs pourrait être la clé de la survie, non seulement pour les
    PJ, mais aussi pour le monde entier !

Habitants dangereux
    Rise of the Runelords met les personnages joueurs face à des centaines
    de menaces monstrueuses. Un personnage préparé doit être adapté pour
    défier des humanoïdes monstrueux, des géants, des bêtes magiques et des
    morts-vivants, et des menaces encore plus importantes à des niveaux plus
    élevés, tels que des étrangers maléfiques et des dragons.

Explorateurs
    Au fur et à mesure que la campagne se déplace à travers la frontière
    de la Varisie, les personnages joueurs rencontrent des ruines perdues.
    Des personnages dotés de compétences appropriées pour s'aventurer dans
    des donjons et des ruines en ruine peuvent donner un avantage au groupe.
    La connaissance (dungeoneering) et la vision raciale en basse lumière
    ou darkvison seraient utiles dans ces endroits humides et sombres.
    Étant donné que bon nombre de ces emplacements ont été protégés et sécurisés
    par leurs constructeurs, un personnage qui peut contourner certaines
    de ces protections pourrait bien aider le groupe à vivre pour se battre
    un autre jour.

Chasseurs de géants
    Les rumeurs de géants se mobilisant dans les contrées sauvages de la
    Varisie se répandent, et les habitants de cette terre frontalière ont
    besoin de sauveurs. Les personnages joueurs prêts à affronter des menaces
    comme celle-ci sont bien adaptés à ce parcours d'aventure.

Perdus dans la brousse
    Alors que certaines parties de ce parcours d'aventure plongent profondément
    dans les ruines, une grande partie de l'action est à la surface. Étant
    donné que la Varisie est une contrée sauvage, les personnages qui sont
    bons pour faire face aux menaces naturelles et excellent à trouver leur
    chemin dans les forêts et les montagnes seraient des atouts pour un groupe
    d'aventuriers. De nombreuses opportunités de voyages par voie terrestre
    abondent également, donc les montures peuvent s'avérer utiles pour amener
    les personnages au seuil de l'aventure, mais peuvent être difficiles
    à gérer une fois que l'aventure se déplace sous terre.


Traits de campagne
==================

Les personnages créés pour cette campagne doivent avoir des raisons d'être
dans la ville de Sandpoint, et encore mieux s'ils ont un intérêt direct
à la défendre. En plus des traits de campagne suivants, le
`Pathfinder RPG Advanced Player's Guide` propose une sélection de traits
de campagne de la campagne "`Shadows under Sandpoint`" de James Jacobs qui
seraient également appropriés pour une utilisation dans cette campagne
(voir page 330 de ce livre). Les autres traits de cette section
(Guide du joueur avancé 326) peuvent également être utilisés pour des
sélections de traits supplémentaires.


.. rule:trait:: Eager Performer

    En apprenant que Sandpoint avait un théâtre rivalisant avec ceux que
    l'on trouve dans les grandes villes comme Magnimar et Korvosa, vous
    avez décidé de tenter votre chance pour y passer du temps. Après avoir
    envoyé une lettre à Cyrdak Drokkus demandant une audition et ne pas avoir
    eu de réponse, vous avez décidé de vous rendre à Sandpoint et de le
    rencontrer en personne, faisant confiance à votre force de volonté et
    à votre charmante influence pour obtenir ce que vous voulez. Vous gagnez
    un bonus de trait de +1 aux tests de n'importe quelle compétence de
    performance. De plus, choisissez n'importe quel sort de l'école
    d'enchantement; son DC de sauvegarde augmente de +1.


.. rule:trait:: Liens familiaux

    Bien que n'étant pas ethniquement Varisien, vous avez été élevé parmi
    les Varisiens et ils vous considèrent comme l'un des leurs. De plus,
    vous avez réussi à vous entendre avec un groupe de Sczarni et à les
    considérer comme votre nouvelle famille. Après avoir été chassé du dernier
    endroit où campait votre famille Sczarni, vous avez retrouvé un ami
    de la famille à Sandpoint, un voyou impitoyable nommé Jubrayl Vhiski
    au Fatman's Feedbag. Pendant votre séjour chez les Sczarni, vous avez
    appris quelques ficelles du métier. Vous gagnez un bonus de trait de
    +1 aux tests de Connaissances (locales) et Connaissances (locales) est
    toujours une compétence de classe pour vous. De plus, vous commencez
    à jouer en sachant parler et lire le varisien.


.. rule:trait:: Amis et ennemis

    L'un des membres de votre famille, peut-être un parent, un cousin, une
    tante ou un oncle, a aidé Daviren Hosk à abattre un groupe de gobelins
    près de Sandpoint. Depuis lors, le membre de votre famille est décédé,
    mais pas avant de vous avoir parlé de ce jour et de l'offre que Daviren
    lui a faite si jamais elle en avait besoin. Une fois que vous êtes arrivé
    à Sandpoint et que vous avez rencontré Daviren Hosk aux écuries de squash
    gobelin, il vous offre l'un de ses meilleurs coursiers et tous les
    accessoires nécessaires en guise de remerciement pour l'aide apportée
    aux membres de votre famille : un cheval entraîné au combat, une selle
    militaire, des sacoches , mors et bride, un mois de nourriture et une
    écurie à vie aux écuries Goblin Squash Stables.


.. rule:trait:: Tueur de géants

    Le village de votre famille a été pillé par des géants dans la nature
    sauvage de la Varisie, ne laissant rien d'autre qu'une ruine fumante.
    Après la destruction de votre village, votre famille s'est entraînée
    au combat contre les géants pour éviter qu'une telle tragédie ne se
    reproduise. Depuis que vous avez entendu parler de géants se mobilisant
    dans toute la campagne, vous vous êtes aventuré à Sandpoint pour aider
    la ville à se préparer à une éventuelle incursion. Vous gagnez un bonus
    de trait de +1 aux tests de Bluff, de Perception et de Motivation et
    un bonus de trait de +1 aux jets d'attaque et de dégâts contre les
    créatures du sous-type géant.


.. rule:trait:: Goblin Watcher

    Vous avez grandi à Sandpoint en regardant de la falaise à travers le
    golfe Varisian. En passant autant de temps à Junker's Edge à regarder
    les gobelins ci-dessous alors qu'ils fouillaient les déchets mis au
    rebut et à voir ce qu'ils faisaient des ordures, vous avez développé
    un œil pour repérer les objets mis au rebut les plus utiles et les plus
    précieux. Vous gagnez un bonus de trait de +1 aux tests de Perception
    et d'Évaluation, et un bonus de trait de +5 aux tests d'Évaluation pour
    déterminer l'objet le plus précieux visible dans un trésor.


.. rule:trait:: Hagfish Hopeful

    Depuis que vous êtes passé par Sandpoint quand vous étiez enfant et
    que vous avez entendu parler du concours dans la taverne populaire
    connue sous le nom de Hagfish, vous vouliez prendre ce porte-monnaie
    comme le vôtre et graver votre nom sur la poutre du plafond au-dessus
    du bar. En vous entraînant à avaler de la nourriture indigeste et à
    boire de l'eau qu'un cochon refuserait, vous avez développé une résistance
    assez forte à tout ce qui est putride et grossier. Vous bénéficiez d'un
    bonus de trait de +2 aux jets de Vigueur contre les maladies et le poison.


.. rule:trait:: Famille marchande

    Vous êtes apparenté à l'une des quatre familles nobles de Magnimar qui
    ont fondé la Ligue marchande de Sandpoint. Soit vous avez grandi à Magnimar
    en tant que cousin de la famille Valdemar ou Deverin, soit vous êtes
    né et avez grandi à Sandpoint. L'éducation à la gestion d'une entreprise
    et des années à s'occuper de l'entreprise familiale vous ont donné le
    sens du commerce. Vous augmentez la limite de GP de tout règlement de
    20 % et pouvez revendre des objets à 10 % supplémentaires par rapport
    au montant de GP que vous obtiendriez normalement en vendant un trésor.


.. rule:trait:: Chasseur de monstres

    Peut-être êtes-vous venu dans le golfe de Varisie à la recherche du
    Diable de Pointe-de-Sable, ou peut-être avez-vous suivi les récits de
    pêcheurs de Old Murdermaw. Quoi qu'il en soit, vous vous êtes aventuré
    à travers la Varisie pour traquer des monstres célèbres. Bien qu'ils
    vous aient tous échappé jusqu'à présent, vous vous êtes rendu à Sandpoint
    pour rechercher et vous réapprovisionner avant de retourner dans la
    nature. En raison de votre entraînement, vous gagnez un bonus de trait
    de +1 aux jets d'attaque et aux jets de dégâts des armes contre les
    aberrations et les bêtes magiques.


.. rule:trait:: Scholar of the Ancients

    Ayant grandi avec le nez dans les livres, vous avez eu un grand intérêt
    pour les cultures du passé et l'histoire ancienne. De plus, ayant grandi
    en Varisie, vous savez que les monuments qui parsèment le paysage
    appartiennent à une ancienne civilisation connue sous le nom de Thassilon.
    À partir de votre vie d'étude et de recherche acharnée, vous avez
    reconstitué la langue et l'histoire partielle de ce grand empire autrefois.
    Vous gagnez un bonus de trait de +1 aux tests de Connaissances (arcanes)
    et Connaissances (histoire) et commencez à jouer en sachant parler et
    lire le Thassilonien.


.. rule:trait:: Etudiant de la Foi

    Alors que vous avez personnellement consacré votre vie à une seule divinité,
    vous étudiez les religions et croyances mortelles. En apprenant que la
    ville de Sandpoint a récemment achevé une cathédrale dédiée aux six
    divinités les plus populaires de la région, vous avez dû voir l'endroit
    par vous-même et êtes arrivé à temps pour la consécration de cet édifice
    sacré. En raison de votre foi solide et de votre large éventail d'études,
    vous lancez tous les sorts de guérison au niveau de lanceur de sorts +1,
    et chaque fois que vous canalisez de l'énergie, vous gagnez un bonus de
    trait de +1 au DC de sauvegarde de votre énergie canalisée.


Varisie, le berceau des légendes
================================

Parsemée des reliques monolithiques d'un empire depuis longtemps effondré,
la Varisie est une terre rude mais majestueuse, ses forêts brumeuses et ses
plaines vallonnées bordées de pics acérés et de mers abondantes. Ses habitants,
récemment libérés du colonialisme, sont des gens de la frontière robustes
et des nobles de l'argent frais, tous désireux de se tailler des noms dans
le paysage austère de la Varisie. Pourtant, au-delà des frontières de leurs
villages, des bêtes et des géants peu habitués à l'empiètement de la
civilisation rôdent dans les collines et les bois, faisant une courte tâche
aux imprudents et aux audacieux.

Ce qui suit est un index géographique de la région connue sous le nom de
Varisie. Bien qu'une grande partie de cette terre accidentée reste inexplorée,
ce que l'on sait peu tente les audacieux et recèle le potentiel d'une aventure
incalculable. Pour les chasseurs de fortune, la Varisie est une terre
d'opportunités illimitées, ses monuments antiques leur rappelant à quel point
les vrais poussés peuvent s'élever.


.. note:: Quand un lieux est plus décris et joué, on déplace sa définition
    dans la documentation du module approprié.  Ainsi, si un lieux n'est
    plus là, vous pourrez le trouver dans :ref:`l'index dédié <world-index>`.
    ou dans :ref:`l'index général <genindex>`.


.. world:location:: Abken

    La ville d':world:loc:`Abken` a été fondée sur une croyance : qu'avec le bon mélange
    de personnes, une ville peut fonctionner comme une seule famille, sans
    individu meilleur qu'un autre. Composée à l'origine de quelques familles
    de la sous-classe Korvosan, cette simple commune agricole s'est développée
    lentement, avec de nouveaux membres admis uniquement par mariage ou par
    vote majoritaire. Bien qu'amical pour les étrangers, le village désormais
    important reste insulaire et discret sur ses processus internes, et la
    grande palissade en rondins autour du complexe principal protège son
    intimité. Les étrangers qui causent des problèmes peuvent s'attendre
    à être maîtrisés en peu de temps, car chaque homme, femme et enfant
    est prompt à lever les bras pour défendre sa « famille ».

.. world:location:: Ashwood

    Alors que de nombreuses forêts de Varisie ont une sombre réputation,
    celle d'Ashwood est légendaire. Tout le monde dans un rayon de cent
    milles prétend avoir un parent ou un ami d'un ami qui a personnellement
    rencontré un fantôme, un loup-garou ou un autre fantôme à l'intérieur
    des frontières maussades du bois. Pourtant, alors que les habitants
    peuvent se vanter avec enthousiasme et échanger des histoires le jour,
    la nuit, ils barrent leurs portes et empilent du bois de chauffage.
    L'église d'Erastil prend ces histoires particulièrement au sérieux,
    et les fidèles d'Old Deadeye sont fréquemment vus patrouiller dans les
    vallons et les villes le long de la lisière déchiquetée de la forêt,
    s'assurant que les créatures sombres à l'intérieur de ses frontières
    y restent.


.. world:location:: Baslwief

    Baslwief est l'une des principales villes minières de la région de
    Korvosan ; ses habitants extraient du fer, du cuivre et des métaux plus
    rares des contreforts des montagnes Fenwall et les expédient en aval.
    En plus des prospecteurs humains, la ville compte une importante
    population de demi-lings, qui trouvent l'esthétique de la frontière
    de la ville très à leur goût.


.. world:location:: Biston

    Ici, les rives du lac Syrantula s'élèvent de l'eau dans une grande falaise
    de pierre en surplomb. Couvrant l'escarpement se trouve une communauté
    ancienne et en ruine, ses grottes creusées dans la roche pour former
    un dédale confortable et interconnecté d'échelles et de cordes. Bien
    que la ville soit actuellement habitée principalement par des pêcheurs
    et des agriculteurs, ses architectes d'origine auraient été une tribu
    de harpies aujourd'hui éteinte.


.. world:location:: Bloodsworn Vale

    Site d'un engagement sanglant entre les forces d'invasion du Chélish
    et des barbares Shoanti désespérés, Bloodsworn Vale était une route
    commerciale principale entre le Cheliax et ses colonies varisiennes.
    Il est tombé en désuétude après l'effondrement de l'empire. Alors que
    quelques villes portuaires varisiennes s'enrichissent de plus en plus
    grâce au commerce du sud, de nombreuses villes enclavées ont commencé
    à réclamer la réouverture du col.


.. world:location:: Brinewall

    Installée à l'origine par des Chelaxians de Korvosa, et autrefois la
    forteresse chélienne la plus septentrionale de Varisie, cette forteresse
    était parfaitement située pour se défendre et faciliter le commerce
    avec les durs guerriers des terres des rois de Linnorm. Malgré la menace
    constante des barbares de Nolander, labarre de dragons qui milice à
    laoccupait le mur-rideau éponyme du fort s'est avérée plus que capable
    de repousser les attaques. Il y a vingt ans, cependant, toute
    communication avec la forteresse a soudainement cessé. Les investigations
    ont révélé une citadelle vide, dépourvue de toute preuve d'attaque ou
    de catastrophe. Bien que la plupart blâment les Nolanders, l'absence
    totale de corps et la forme immaculée des navires vides flottant dans
    le port évoquent une calamité plus sinistre.


.. world:location:: Monts Calphiak

    Les monts Calphiak sont la plus jeune chaîne de Varisie, datant d'à
    peine 10 000 ans jusqu'à la fin cataclysmique de l'empire thassilonien.
    Aujourd'hui, les montagnes sont réputées pour leur forte concentration
    d'artefacts thassiloniens, notamment dans la Vallée des étoiles, un
    cratère fortement gravé que de nombreux explorateurs pensent être un
    observatoire céleste massif.


.. world:location:: Celwynvian

    Au fond de la forêt de Mierani, l'ancienne capitale elfique de Celwynvian
    est vide, ses palais verdoyants et ses tours délicates suspendues à
    bout de souffle dans la pénombre sous la canopée. Évitée par superstition
    par les autres races pendant la longue absence des elfes, la Cité des
    Pluies d'Émeraude est mise en quarantaine depuis leur retour. Refusant
    toutes les demandes des étrangers d'entrer dans leur maison ancestrale,
    les elfes prétendent avoir coupé la ville pour offrir à leur espèce
    un refuge contre le monde extérieur. Ceux qui traitent avec les habitants
    de la forêt de Mierani, cependant, murmurent que les elfes eux-mêmes
    résident en réalité en dehors de la ville, menant une guerre cachée
    pour reprendre leur capitale à une force sinistre et sans nom.


.. world:location:: Tombeau de Chorak

    Les géants du plateau de Storval n'ont pas toujours été les barbares
    qu'ils sont aujourd'hui, et la meilleure preuve en est peut-être sur
    la petite île du lac Skotha connue des géants sous le nom de Tombe de
    Chorak. Ceci reste la spéculation, cependant, car toutes les créatures
    sensibles qui tentent de s'approcher de l'île sont bombardées de rochers
    ou abattues avec des boulons de baliste sculptés par des runes. Même
    les autres géants ne savent pas quels secrets se cachent au centre de
    l'île, et pour l'instant, au-delà de quelques reflets de structures
    métalliques au loin, le mystère de la tombe de Chorak reste sans réponse.


.. world:location:: Churlwood

    Une forêt enchevêtrée étouffée par des vignes tenaces, Churlwood est
    presque impossible à naviguer pour les non-autochtones, ce qui en fait
    le refuge idéal pour les gangs de bandits et les tribus de gobelins
    qui pillent ses frontières. Avec son gibier abondant et sa capacité
    renommée à dérouter même les pisteurs les plus rusés, le bois est une
    destination populaire pour les criminels recherchés, ses frontières un
    refuge pour les voleurs et les esclaves en fuite, d'où l'expression
    « En sécurité comme un voleur à Churlwood ». Bien sûr, ce que les histoires
    de héros folkloriques hors-la-loi et de bandits égalitaires omettent
    de mentionner, c'est combien d'entre eux entrent dans la forêt pour
    ne plus jamais être revus.


.. world:location:: Cinderlands

    Les Cinderlands occupent la majorité du sud du plateau de Storval, son
    sol sec et cendré approchant le statut de désert à de nombreux endroits.
    Beaucoup de plantes ici ont besoin du feu pour ouvrir leurs gousses,
    et en été, les incendies de forêt traversent les badlands dans de vastes
    nappes de flammes allumées par les violents orages saisonniers. Dans
    ces environs rudes, seuls les Shoantis s'installent réellement, et ceux-ci
    se composent généralement de yourtes et d'autres structures facilement
    transportables. Le feu joue également un rôle central dans la vie de ces
    tribus des hautes terres, et beaucoup favorisent des rites de passage
    difficiles au cours desquels les jeunes guerriers doivent dépasser un
    feu de forêt ou abattre un animal conduit devant les flammes.


.. world:location:: Crystalrock

    découverte à l'origine par les nains de Janderhoff, qui l'appellent
    parfois le « Cœur du monde », cette formation cristalline massive est
    suspendue à des fils de cristal d'apparence frêle dans une caverne
    naturelle loin sous le bord des montagnes Mindspin. Pendant des centaines
    d'années, les anciens nains se sont réunis ici chaque année pour regarder
    le cristal se convulser soudainement, envoyant une impulsion vibrante
    profonde qui peut être ressentie dans les os des créatures à des kilomètres
    à la ronde. Récemment, cependant, les nains qui étudient Crystalrock
    se sont retirés avec inquiétude et excitation alors que les coups ont
    commencé à s'accélérer, coïncidant actuellement avec le changement des
    saisons.


.. world:location:: Crystilan

    Bien que son nom d'origine ait été perdu depuis longtemps, le site appelé
    Crystilan est aujourd'hui l'un des artefacts thassiloniens les plus connus
    et a fourni aux érudits une grande partie de ce qu'ils savent maintenant
    sur la vie thassilonienne. Visible depuis la mer, le dôme brillant de
    cristal translucide est magnifique à voir, captant la lumière du soleil
    et le rendant trop brillant pour être regardé directement. De près,
    les aventuriers peuvent scruter à travers le cristal lisse, presque
    sans friction, le morceau de la ville à l'intérieur, parfaitement conservé
    comme une mouche dans l'ambre. Bien que beaucoup aient tenté de percer
    et d'atteindre les grands temples à gradins et les vastes arches, aucune
    magie ou arme actuellement connue n'a jamais pu endommager ou pénétrer
    le cristal, et ceux qui ont tenté de creuser sous celui-ci pensent que
    l'étrange bouclier est une sphère parfaite. Pour l'instant, au moins,
    la plupart des érudits se contentent de transcrire les runes visibles
    et observent la ville étrangement déserte alors qu'elle progresse,
    immuable, à travers les âges vers un but inconnu.


.. world:location:: Curchain Hills

    Les creux et les vallons herbeux des Curchain Hills abritent des tribus
    relativement pacifiques de Shoanti, de grands troupeaux d'aurochs au
    pâturage et plusieurs familles superstitieuses de peuples frontaliers.
    Les voyageurs à travers la région prétendent souvent que certaines collines
    semblent trop similaires, suggérant une formation non naturelle.


.. world:location:: Lac de Braise

    Vraisemblablement alimenté par des sources chaudes ainsi que par les
    rivières Lampblack et Malgorian, les eaux du lac de Braise s'élèvent
    chaudement des plaines vallonnées, et certains endroits le long du rivage
    fument pendant les mois les plus froids. Le lac abrite également d'étranges
    créatures aquatiques qui s'élancent comme un feu dans d'immenses bancs
    sous la surface. Pas tout à fait des poissons, ces minuscules créatures
    appelées « charigs » ressemblent à des salamandres, leur peau transparente
    phosphorescente dans les eaux claires. Bien que les créatures semblent
    inoffensives, les habitants évitent de les manger, affirmant que certaines
    nuits de l'année, les écoles se rassemblent en motifs scintillants sur
    des kilomètres de large, se déplaçant avec détermination et intelligence,
    comme si elles créaient des signaux lumineux visibles uniquement du ciel.


.. world:location:: Montagnes Fenwall

    Bien que peu d'entre eux tentent d'établir de véritables fermes
    dans les montagnes Fenwall infestées de monstres, les riches filons de
    fer et de métaux précieux des sommets en font une destination
    attrayante pour les Korvosans de la classe inférieure et les indésirables qui
    cherchent à frapper gros en tant que prospecteurs. En tant que telles, les
    vallées des Fenwalls sont parsemées de minuscules camps d'une personne
    ainsi que d'excavations minières à ciel ouvert fortement surveillées
    financées par de plus grandes entreprises minières basées à Korvosa.


.. world:location:: Montagnes de Fogscar

    Les gobelins denses des montagnes de Fogscar se chamaillent
    constamment, chaque ravin pierreux semblant abriter une nouvelle tribu de
    charognards mordants. Bien que des routes étroites passent entre les sommets
    brumeux, il est conseillé aux voyageurs à la recherche d'un raccourci entre
    Magnimar et Roderic's Cove d'être bien armés et de transporter de grands
    magasins de bibelots bon marché - ou d'ordures moyennement utiles - avec
    lesquels acheter les indigènes notoirement cupides.


.. world:location:: Galduria

    Alors que la ville de Galduria survit principalement en transportant du
    grain et du bois le long de la rivière Lampblack et du lac Ember, son
    véritable titre de gloire est son université. De loin la plus ancienne
    structure de la ville, l'Académie du Crépuscule est l'une des
    principales écoles de magie de Varisie, n'ayant pour rivale que la
    Pierre des Seers de Magnimar et les célèbres Acadamae de Korvosa, qui
    la considèrent tous deux comme un arriviste dépourvu de la leur.
    riches patrimoines. Fondée à Galduria spécifiquement pour éviter la
    pression politique et les intrigues de ces deux villes, la Twilight
    Academy a la réputation d'être expérimentale et non conventionnelle
    dans bon nombre de ses pratiques, mais les dons fréquents aux travaux
    publics empêchent les habitants de sonder trop profondément dans le
    sort de dérèglement occasionnel. ou accident nécromantique.


.. world:location:: Les Gnashers

    foyer traditionnel de plusieurs tribus de géants des collines, les
    Gnashers offrent au courageux explorateur un rare aperçu du vaste
    empire géant qui a précédé l'établissement humain en Varisie.
    Malheureusement, la nature brutale de ses résidents actuels fait au
    mieux de telles expéditions un pari dangereux.


.. world:location:: Gruankus

    Son but initial est inconnu, la grande roue de pierre de Gruankus se
    trouve sur le rivage du golfe Varisien, sa masse gravée de runes à
    moitié enfouie dans le sable. Aujourd'hui, on s'en souvient mieux comme
    le site des négociations entre Riddleport et les diplomates Magnimaires
    qui ont conduit au Traité de Gruankus, qui a gardé les commerçants du
    golfe Varisien à l'abri des attaques de pirates pendant près de cent
    ans.


.. world:location:: Guiltspur

    Même les sauvages géants qui dominent le nord du plateau de Storval
    évitent Guiltspur, une excavation de plus en plus profonde dans le
    passé, creusée par la sueur géante et la griffe de dragon. Non
    marqué et inconnu pendant des siècles, ce n'est que récemment que le
    site s'est révélé être une catacombe thassilonienne
    complexe.


.. world:location:: Harse

    ce village est perché sur la langue de terre où se rencontrent les
    rivières Sarwin et Falcon, et les ferries jumeaux Harse sont le moyen
    le plus simple pour les voyageurs de la région de traverser l'une ou
    l'autre des grandes rivières. En outre, Harse possède les meilleurs
    éleveurs de chevaux et de bétail au sud des hautes terres de Velashu,
    et organise chaque année un énorme rodéo conçu pour distinguer les
    meilleurs animaux et cavaliers.


.. world:location:: Hollow Mountain

    Sur le plus grand des pics déchiquetés de l'île de Rivenrake, ricane
    le visage monumental et brisé d'une femme à l'air sévère,
    renfrognée toujours vers le sud sur les vestiges de l'ancien pont
    titanesque qui reliait Rivenrake à l'île d'Argavist. Coupé
    verticalement en deux, le visage révèle des niveaux sur des niveaux
    d'architecture exposée au sein de la montagne, tandis qu'en dessous,
    les fondations en ruine d'une tombe étouffée par la poussière d'une
    ville grimpent à flanc de montagne, tentant les aventuriers avec la
    promesse de découvertes incalculables.


.. world:location:: Hook Mountain

    Cette montagne massive se trouve à l'extrémité sud des pics de fer et
    domine la forêt de Sanos. Les chasseurs et les trappeurs de la région
    évitent la zone car ils prétendent que des tribus sauvages d'ogres
    consanguins habitent l'endroit.


.. world:location:: Ilsurian

    Dans les années qui ont immédiatement suivi l'effondrement de l'Empire
    Chelish, Korvosa était en proie à la tourmente, avec diverses maisons
    nobles et représentants du gouvernement se disputant l'endroit où
    tomberait l'allégeance de la colonie. Alors que de nombreux fidèles à
    l'ancien empire ont finalement quitté Korvosa et se sont installés à
    Magnimar, la leur n'était pas la seule faction à déserter la ville en
    conflit. Ilsur, anciennement une première épée parmi les chevaliers
    d'Aroden, a préconisé de se débarrasser complètement du règne de la
    noblesse et de restructurer Korvosa en une méritocratie militaire
    efficace. Il a fait campagne pendant des années mais a concédé
    l'échec en 4631 ar avec la fondation de la maison royale Korvosan.
    Ilsur mena ses troupes jusqu'à la côte du lac Syrantula, où ils
    s'installèrent dans un petit village de pêcheurs et se retranchèrent
    pour attendre leur chance de revenir et mettre la nouvelle aristocratie
    au fil de l'épée. Bien qu'Ilsur soit mort depuis longtemps et que ses
    descendants soient plus des bûcherons et des poissonniers que des
    soldats, le village reste farouchement indépendant - ne cédant ni à
    Korvosan ni à la règle Magnimarian - et tous les habitants de la ville
    sont tenus de maintenir une épée tranchante et de s'entraîner contre
    le jour où ils pourraient avoir à le faire. défendre leur liberté
    contre les tyrans.


.. world:location:: Les pics de fer

    bien que les fermes situées dans les vallées le long de leur bordure
    sud-ouest bénéficient des fortes pluies qui tombent du côté ouest
    des montagnes, les pics de fer sont réputés pour être le domaine des
    ogres, des géants des collines et des géants de pierre facilement
    irritables. Dans la mesure du possible, les habitants évitent de
    s'aventurer au-delà des contreforts de la chaîne et conseillent aux
    voyageurs de faire de même.


.. world:location:: Janderhoff

    Avec son mur rideau de fer massif et ses clochers en cuivre battu, la
    forteresse naine de Janderhoff s'accroupit comme une grande bête
    blindée parmi les contreforts des montagnes Mindspin. Pourtant, malgré
    son apparence rébarbative, la ville est un nœud commercial animé, les
    Shoanti et les Chelaxiens traversant les tunnels bien gardés qui
    forment les seules entrées de la ville. Une fois à l'intérieur, les
    visiteurs se retrouvent rapidement dans les marchés à plafond bas et
    les forges qui font vivre la ville. Ces bâtiments de surface sont
    principalement destinés à recevoir des étrangers, car la majorité de
    la population de la ville vit sous terre dans un réseau complexe de
    rues souterraines.


.. world:location:: Kaer Maga

    La Cité des Étrangers, comme on appelle aussi Kaer Maga, est une ville
    anarchique au sommet d'une falaise construite à l'intérieur des ruines
    d'un ancien monument. La ville est connue comme un endroit où quelqu'un
    peut se cacher de la persécution ou commencer une nouvelle vie au
    milieu de dizaines de factions étranges et de milliers de ses habitants
    uniques.


.. world:location:: Montagnes Kodar

    Grands et intimidants, les sommets enneigés déchiquetés des montagnes
    Kodar sont parmi les plus hauts du monde. Seules les créatures les plus
    robustes, telles que les géants des tempêtes, les rocs et les dragons,
    sont capables de résister au climat extrême et aux falaises
    dangereuses. De nombreux mystères et légendes ont des origines
    cachées au plus profond des montagnes Kodar, comme le monastère
    chimérique de l'esprit du paon, la citadelle des nuages ​​de
    Chadra-Oon et la cité perdue de Xin-Shalast.


.. world:location:: Korvosa

    Ancienne capitale de la Varisie coloniale, Korvosa a subi plusieurs
    années de troubles après la chute de l'empire chélish et est sortie
    de cette lutte à peu près équivalente à Magnimar en termes de
    pouvoir politique, un fait qui irrite encore de nombreux habitants de
    Korvosa. De nos jours, la plupart des nobles décadents de Korvosa
    continuent de mettre en valeur leurs liens avec le Chéliax et leur
    approbation des modes du sud, se considérant comme le centre de la
    culture et des lumières de Varisie. Que de telles vanités soient
    exactes ou non, la ville est certainement plus étroitement liée
    qu'aucun de ses voisins au passé impérial du pays.


.. world:location:: Le Lady's Light

    Penché de manière précaire au bout d'une flèche rocheuse, le Lady's
    Light est le premier aperçu de Varisia par un marin du sud. Haut de
    près de 200 pieds, l'énorme phare de pierre a la forme d'une femme
    sensuelle vêtue d'une robe ample qui laisse un sein nu, sa main droite
    pointant un bâton qui projette un faisceau de lumière brillant vers la
    mer à intervalles réguliers. À la base de la statue, une gigantesque
    porte en pierre aux charnières étranges mène vraisemblablement à
    l'intérieur du phare, mais personne n'est connu pour l'avoir
    déverrouillée à l'époque moderne.


.. world:location:: Lac Skotha

    Ce lac est considéré comme sacré par les géants des collines locaux,
    qui refusent de le visiter sauf pendant les funérailles. Chaque fois
    que l'un d'entre eux meurt, il est placé sur une barge qui est ensuite
    incendiée et poussée sur l'eau, afin que son esprit puisse rejoindre
    ses ancêtres sur la mystérieuse île centrale. Les géants n'aiment
    pas les étrangers, mais ceux qui pénètrent dans le lac trouvent son
    fond recouvert d'une épaisse couche d'os géants.


.. world:location:: Lac Stormunder

    Le lac Stormunder tire son nom des vastes geysers sous-marins qui
    bouillonnent et bouillonnent dans ses profondeurs. Sur ses rivages, les
    pêcheurs sont parfois obligés de se réfugier alors que les rochers
    survolent la surface à la recherche de proies.


.. world:location:: Lac Syrantula

    L'une des voies navigables les plus fréquentées de Varisie, ce lac de
    cent milles de long est une partie principale de la route commerciale
    entre Korvosan et les possessions Magnimarian. Bien que la plupart des
    pêcheurs et des marins qui sillonnent ses eaux n'aient guère plus à
    craindre que les gars géants qui sont une source de nourriture majeure
    pour les communautés voisines, personne ne peut vraiment dire quelles
    bêtes pourraient dormir dans un plan d'eau aussi énorme, et la plupart
    qui vivent le long de ses rives prennent soin d'éviter les ruines
    mystérieuses qui parsèment sa frontière sud.


.. world:location:: Lurkwood

    Autrefois le foyer d'innombrables elfes, Lurkwood est désormais
    résolument évité par ses anciens protecteurs pour des raisons qu'ils
    refusent de nommer. Les habitants murmurent que la forêt s'est
    détachée de la marche des années, et des histoires circulent de
    voyageurs qui s'y sont égarés, pour en sortir beaucoup plus jeunes ou
    plus âgés qu'ils ne devraient l'être. Une chose est sûre : les
    saisons à Lurkwood ne semblent pas correspondre à celles de la terre
    qui l'entoure - ses feuilles changent de couleur et tombent même
    lorsque les champs des agriculteurs sont neufs et verts.


.. world:location:: Magnimar

    Fondée par d'anciens Korvosans cherchant à se débarrasser de la
    domination chélienne et à former une métropole démocratique, la
    célèbre Cité des Monuments se trouve à la pointe sud du golfe
    Varisien, construite autour de l'un des derniers vestiges de l'énorme
    pont qui s'étendait autrefois jusqu'à Hollow Mountain.


.. world:location:: Montagnes Malgoriennes

    Bien que les éleveurs aventureux en fassent l'une des chaînes de
    montagnes les plus civilisées de Varisie, c'est aussi l'une des plus
    géologiquement actives. Bien qu'il contienne peu de volcans de toute
    taille réelle, des geysers, des sources chaudes et des fosses de
    goudron bouillonnant parsèment la chaîne, remplissant les montagnes de
    nuages ​​étranges et parfois étouffants et rendant les voyages
    dangereux pour ceux qui ne sont pas habitués à de tels dangers. Bien
    qu'ils soient fermés à de telles choses par rapport aux autres races,
    les gnomes de la forêt de Sanos semblent particulièrement attirés par
    ces caractéristiques géologiques, et des troupes du petit peuple
    peuvent parfois sonner dans un geyser longtemps après le coucher du
    soleil, effectuant une sorte de prière ou de rituel privé.


.. world:location:: Melfesh

    La ville de Melfesh s'étend sur de longues jetées à travers le
    Yondabakari, et le courant de la rivière fait tourner les nombreuses
    grandes roues hydrauliques qui alimentent les moulins à céréales et
    à bois de la ville. Un vaste pont-levis au centre de la ville s'élève
    et s'abaisse, permettant à la ville de prélever un péage sur tout
    navire ou caravane souhaitant passer - une pratique qui ne gagne pas
    l'amour des capitaines qui commercent sur ce tronçon de la
    rivière.


.. world:location:: Forêt de Mierani

    Le foyer régional des elfes depuis des temps immémoriaux, la forêt de
    Mierani est un lieu d'arbres énormes et étendus et d'une faune
    abondante. Les elfes Mierani maintiennent la forêt comme une nature
    sauvage civilisée, permettant au cours de la nature de se dérouler
    sans entrave et la protégeant des menaces monstrueuses et des intrus
    brandissant des haches. Alors que de petites communautés elfes gardent
    la lisière boisée et que des nés sauvages errants patrouillent dans
    les profondeurs, les elfes sont toujours en train de nettoyer la forêt
    après leur absence de plusieurs siècles, leurs carrières les plus
    persistantes étant des tribus d'ettercaps, des plantes voraces et un
    singulier insaisissable. dragon vert connu sous le nom de Razorhorn.
    L'enclume de Minderhal : Cette ancienne montagne-temple s'élève en
    l'honneur de Minderhal, le dieu forgeron maussade des géants. Au-delà
    de ses piliers de marbre craquelés, l'immense forge-autel est toujours
    debout, sa fournaise froide autrefois alimentée par les corps des
    contrevenants condamnés. Peu de géants viennent maintenant ici, et la
    statue de pierre du géant seigneur du jugement est assise sans
    surveillance sur son trône, le menton appuyé sur son poing, regardant
    à travers le Storval Deep. Montagnes Mindspin : Remplies de géants,
    d'ogres et de trolls, sans parler de crevasses profondes et de dangereux
    glissements de terrain, les montagnes Mindspin sont considérées comme
    un piège mortel pour tous, sauf les voyageurs les plus expérimentés.
    Ironiquement, de tels dangers pourraient être l'un des plus grands
    atouts de Varisia, car à ce jour, ils ont empêché les tribus orques
    en guerre du Fort de Belkzen de balayer la chaîne jusqu'à
    Korvosa.


.. world:location:: Le Mobhad Leigh

    Avec un nom Shoanti signifiant « entre en enfer », le Mobhad Leigh a
    captivé l'imagination pendant des siècles. Une fosse parfaitement
    ronde dans un champ autrement indescriptible au pied des montagnes
    Kodar, le Leigh n'a jusqu'à présent jamais été prouvé de manière
    concluante qu'il avait un fond. Des marches en spirale le long des
    parois abruptes de la fosse descendent sur plusieurs centaines de pieds
    avant de s'effondrer, et ceux qui se sont aventurés plus loin ne sont
    jamais revenus pour dire s'ils recommencent ou non plus bas. Les Shoanti
    locaux évitent généralement le Leigh, en particulier après que
    plusieurs de leurs utilisateurs de magie soient morts en essayant
    d'explorer la fosse avec la magie de la divination. Pourtant, certaines
    nuits de l'année, des lumières oranges scintillantes peuvent être
    vues danser au plus profond de ses profondeurs.


.. world:location:: Mundatei

    La forêt d'obélisques de Mundatei n'est pas une vraie forêt. Au
    contraire, lorsque les voyageurs franchissent la crête dans la large
    vallée de Mundatei, leur première impression est celle d'un vaste
    enchevêtrement de menhirs - des milliers et des milliers de pierres de
    10 pieds de haut sculptées par endroits avec des motifs de runes en
    spirale. C'est un spectacle à couper le souffle, et la rumeur dit
    depuis longtemps que certains des obélisques sont creux et contiennent
    des trésors. Mais lorsqu'un groupe d'explorateurs de Korvosan a ouvert
    une douzaine d'obélisques il y a près de cent ans, ils ont découvert
    que chaque obélisque contenait un corps humain tordu et mort depuis
    longtemps, ses membres et son expression figés dans la douleur et
    l'horreur. Ce soir-là, les camps des explorateurs ont été assaillis
    par d'horribles morts-vivants dont la chair était dure comme de la
    pierre et dont les yeux étaient horriblement vivants et frais. Une
    douzaine de personnes ont été emportées par les morts-vivants, et
    lorsque les survivants les ont recherchés le lendemain, aucune trace
    n'a été trouvée, mais les 12 obélisques ouverts s'étaient reformés
    dans leurs formes précédentes comme s'ils n'avaient jamais été
    touchés. Peu sont retournés à Mundatei depuis ce jour
    fatidique.


.. world:location:: Les Mushfens

    Au sud du Yondabakari, la terre devient un enchevêtrement suant de
    marais marécageux et de mangroves impénétrables, des meres sans fin
    et des marais capables d'avaler des hommes sans laisser de trace. Outre
    les dangers habituels du marais, les Mushfens sont connus pour leurs
    populations vicieuses de boggards, de géants des marais et de
    harceleurs sans visage.


.. world:location:: Les Nolands

    Les Nolands sont des plaines rocailleuses et rugueuses où les tribus
    des rois de Linnorm exilent leurs criminels les plus méprisables et les
    plus lâches. Des siècles de cette pratique ont donné naissance à de
    nombreuses bandes de berserkers qui parcourent le pays, s'attaquant les
    uns les autres, massacrant sans pitié et se régalant de la chair de
    leurs ennemis. Alors que les Nolanders sont trop désorganisés pour
    prouver une réelle menace pour la Varisie, les habitants des Hautes
    Terres de Velashu et des Montagnes Rouges sont constamment en garde
    contre les raids sauvages des habitants du Nord. Certaines tribus
    Shoanti bannissent également leurs criminels dans les Nolands, bien
    qu'une telle punition soit généralement considérée comme moins
    honorable qu'une mort pure.


.. world:location:: Nybor

    Réputée pour sa tolérance raciale, cette paisible communauté
    agricole compte un plus grand nombre de métis par habitant que partout
    ailleurs en Varisie et encourage fortement les mariages interraciaux.
    Alors que la ville attire parfois la colère des sectes puritaines,
    nombreuses sont les jeunes femmes nobles Magnimaires qui sont
    tranquillement emmitouflées et expédiées à Nybor lorsque des
    aventures illicites entraînent une grossesse.


.. world:location:: Palin's Cove

    Ici, les eaux claires de la rivière Falcon deviennent brunes et noires
    alors que les usines de Palin's Cove, le centre industriel de Korvosa,
    évacuent les déchets dans son courant. Un développement relativement
    récent, les usines ont attiré une grande animosité et même la
    violence des druides, des adorateurs de Gozreh et même des forgerons et
    artisans ordinaires, mais personne ne peut nier que la qualité des
    produits fabriqués par ces ateliers donne un énorme coup de pouce à
    l'économie de Korvosan.


.. world:location:: Montagnes rouges

    Les Montagnes Rouges sont relativement basses par rapport aux normes
    varisiennes, leur sol rouillé rouge avec d'épais dépôts de fer. Les
    habitants, principalement mineurs et bergers, tirez de maigres
    subsistances des collines arides, se regrouper sur leurs poneys des
    hautes terres hirsutes contre les sans foi ni loi pillards des Nolands.
    Si près des berserkers, des crêtes et les ravins des Montagnes Rouges
    ont un attrait naturel pour les paladins et rangers, qui ont pour vocation
    de protéger la frontière nord de la Varisie. Ces derniers temps, les
    raiders semblent organisés de manière inquiétante, et bon nombre des
    les dirigeants locaux ont commencé à demander de l'aide aux Southlands,
    allant même jusqu'à jusqu'à envoyer des émissaires à Riddleport et aux
    Hellknights stationnés à Magnimar.


.. world:location:: Riddleport

    Port le plus au nord de la Varisie, la tristement célèbre ville de
    Riddleport est réputée comme un refuge pour les scélérats, les parias
    et pire encore. Les coupe-gorge remplissent son bordels du port et des
    quais, avec les officiers de justice de Riddleport étant juste un autre
    gang de voleurs (et à peine le plus puissant à cette). Pourtant, même
    dans un tel repaire d'iniquité et de vice, les savants et les historiens
    abondent, tentant de déchiffrer les runes de la grande arche connu sous
    le nom de Cyphergate, qui enjambe l'embouchure du port et se profile
    sur chaque navire qui entre dans la ville. Bien que tout progrès sur
    le l'inscription a été gardée silencieuse, des fouilles récentes suggèrent
    que le arche massive pourrait en fait n'être qu'un segment d'un anneau
    qui s'étend dans les falaises qui entourent le port.


.. world:location:: Ravenmoor

    Pittoresque et isolé, les habitants de Ravenmoor sont heureux de
    commercer avec ceux qui traversent la rivière Lampblack, mais les
    voyageurs qui cherchent à passer la nuit constatent qu'aucun des les
    auberges de recherche acceptent les pensionnaires. De plus, bien
    qu'apparemment extrêmement pieux, les résidents répugnent à discuter
    de leurs croyances religieuses avec des étrangers.


.. world:location:: Montagnes Rouges

    Les Montagnes Rouges sont relativement basses par rapport aux normes
    varisiennes, leur sol rocheux rouillé rouge avec d'épais dépôts de
    fer. Les habitants, principalement des mineurs et des bergers, gagnent
    de maigres revenus dans les collines arides, se regroupant sur leurs
    poneys hirsutes des hautes terres contre les pillards sans foi ni loi
    des Nolands. Si proches des berserkers, les crêtes et les ravins des
    Montagnes Rouges attirent naturellement les paladins et les rangers, qui
    ont pour vocation de protéger la frontière nord de la Varisie. Ces
    derniers temps, les raiders semblent organisés de manière
    inquiétante, et de nombreux dirigeants locaux ont commencé à demander
    de l'aide aux terres du sud, allant même jusqu'à envoyer des
    émissaires à Riddleport et aux Hellknights stationnés à
    Magnimar.


.. world:location:: Faille de Niltak

    Que la Faille de Niltak ait été ouverte par une grande magie ou une
    calamité géologique, personne vivant aujourd'hui ne peut le dire.
    Remplies de structures étranges et palpitantes et d'une flore étrange,
    les profondeurs embrumées du canyon fourmillent d'horreurs ressemblant
    à des mille-pattes, de prédateurs hurlants ressemblant à des
    chauves-souris et d'autres terreurs. Il convient de noter que les
    descriptions précises des profondeurs sont rendues d'autant plus rares
    par le taux de suicide élevé parmi les quelques explorateurs qui
    reviennent de voyages ci-dessous.


.. world:location:: Sirathu

    Ce hameau est à la fois le plus pauvre et le plus éloigné des
    possessions de Korvosa. Bien que généralement rejetés par leurs «
    chefs » au sud comme de sales paysans, les habitants de Sirathu ont
    récemment attiré l'attention de la ville en se ralliant à un enfant
    de 10 ans qui aurait prédit l'avenir et exhorté à la sécession de la
    corruption de Korvosa. » avant que l'orage éclate.


.. world:location:: Skull's Crossing

    Cet immense barrage thassilonien retient l'eau du Gouffre de Storval.
    Censément construit par d'anciens géants, la structure massive est
    décorée de crânes sculptés dans son visage.


.. world:location:: Spindlehorn

    Des milliers de pieds de haut, Spindlehorn s'élève du rivage du
    Storval Deep comme une aiguille contre le ciel, ses côtés abrupts à
    l'exception de l'ensemble perfide d'escaliers qui serpente autour de la
    flèche tordue jusqu'à ce qu'il atteigne le pic aplati, un espace à
    peine 10 pieds de diamètre. Personne ne sait à quoi servait autrefois
    la mystérieuse flèche, mais les récits racontent que des pèlerins en
    robe sombre ont été vus monter les escaliers vertigineux mais ne
    jamais descendre.


.. world:location:: Spire of Lemriss

    Pas exactement un arbre, le Spire of Lemriss est un énorme épi de
    matière végétale s'étendant sur des centaines de pieds dans le ciel,
    ses côtés presque verticaux recouverts d'une coquille de vignes
    jusqu'aux bras et sa structure interne faite de torsades et tressées
    des troncs de bois jaillissant les uns des autres en une cascade sans
    fin. Dans ses branches, des oiseaux et des rongeurs nichent et se
    reproduisent, leurs hululements résonnaient dans le Churlwood voisin,
    accompagnés du crissement occasionnel de quelque chose de beaucoup plus
    gros. Alors que certains pensent que la flèche est une bouture germée
    de l'arbre du monde, ses véritables origines restent
    inconnues.


.. world:location:: Stony Mountains

    alors qu'il s'agit de l'une des rares chaînes de montagnes relativement
    libres de géants en Varisie, maintenue de cette façon par les
    guerriers Shoanti du Tamiir-Quah, les Stony Mountains sont toujours
    dangereuses pour les imprudents, car les griffons, les manticores, les
    tribus de harpies d'autres créatures dangereuses ont élu domicile dans
    les pics escarpés.


.. world:location:: The Storval Deep

    Remplissant toute la vallée entre les Iron Peaks et les Wyvern
    Mountains, le Storval Deep est un lac massif retenu par un ancien
    barrage, Skull's Crossing, à son extrémité sud. De plus, les rives du
    lac elles-mêmes semblent curieusement travaillées, comme sculptées
    dans la pierre environnante par plus que l'eau. Bien que les rumeurs
    abondent sur les villes englouties, les puits de mine inondés et les
    reliques si puissantes que les anciens ont créé le lac juste pour les
    cacher, peu se sont jamais aventurés dans les profondeurs insondables
    de l'eau sombre.


.. world:location:: Plateau de Storval

    La terre de Varisie est divisée en deux par le Storval Rise, une
    falaise de mille pieds de haut sculptée sur une grande partie de sa
    longueur en d'anciennes statues usées par les intempéries, des
    forteresses de falaises et d'étranges portails vers des profondeurs
    surnaturelles. The Rise sépare les plaines luxuriantes et fertiles des
    terres arides et arides du plateau oriental. Ici, des géants et des
    tribus de Shoanti aux tranchants durs règnent en maître, grattant une
    existence de la surface dure du plateau ou pourchassant des troupeaux
    d'aurochs tonitruants à travers les prairies clairsemées.


.. world:location:: Les escaliers de Storval

    bien que dimensionnés pour un colosse, les escaliers de Storval restent
    la route la plus rapide des basses terres occidentales de la Varisie sur
    le plateau. Ici, là où le Storval Rise se réduit à seulement
    quelques centaines de pieds de falaise verticale, de grands escaliers
    ont été taillés dans la falaise, flanqués de chaque côté
    d'énormes statues. Au cours des milliers d'années qui se sont
    écoulées depuis la sculpture des escaliers, des ingénieurs de moindre
    envergure ont découpé des marches et des rampes plus pratiques et à
    taille humaine dans leurs côtés, des itinéraires capables de gérer
    des pelotons entiers d'explorateurs et d'aventuriers.


.. world:location:: La reine engloutie

    S'enfonçant lentement dans les eaux du marais, cette énorme pyramide
    de pierre est toujours imposante, avec un côté entier sculpté dans un
    bas-relief d'une belle femme nue. Depuis le sommet de la pyramide, de
    nombreuses tours incurvées s'étendent à des angles étranges, comme
    des excroissances ou des cheminées, et les légendes affirment qu'à
    l'intérieur des murs austères de la reine engloutie se trouvent couche
    sur couche de catacombes mortelles remplies des secrets de l'empire
    thassilonien perdu.


.. world:location:: Turtleback Ferry

    Cette colonie isolée sur les rives de la rivière Skull maintient une
    population calme et facile à vivre. Loin des autres grandes
    agglomérations, ce village doit se maintenir car il reçoit peu de
    visiteurs.


.. world:location:: Urglin

    Les tours brisées d'Urglin s'élèvent comme une plaie de la plaine
    foudroyée des Cendrillons. C'était autrefois le site d'une ancienne
    ville, mais les orcs du Fort de Belkzen ont pillé et ruiné tout ce qui
    avait de la valeur au cours de décennies de squat et d'abus,
    construisant sur les fondations en ruine des ghettos délabrés de
    roches, de fer et d'os récupérés. Shoanti exclus, géants, métis et
    autres monstruosités errent dans les rues perfides où la force est la
    seule loi. À travers le centre de la ville s'écoule le léthargique
    Ooze, un ruisseau qui a autrefois donné vie à la ville mais qui est
    maintenant pollué à la consistance d'un pudding par les déchets de la
    ville.


.. world:location:: Golfe Varisien

    Lorsque l'Empire Thassilonien s'est brisé il y a 10 000 ans, une grande
    partie de la Varisie occidentale est tombée dans la mer, devenant ce
    que l'on appelle aujourd'hui le golfe de Varisie. Bien que seuls Hollow
    Mountain et quelques vestiges de la grande Irespan de Magnimar restent
    comme des rappels des nations perdues dans les vagues, ces sauveteurs
    qui gagnent leur vie en explorant le fond de la mer ici le trouvent en
    proie à des villes en ruines et à d'anciennes reliques d'une taille
    extraordinaire.


.. world:location:: Hautes terres de Velashu

    Les seigneurs des chevaux des hautes terres de Velashu sont largement
    considérés comme les meilleurs éleveurs de chevaux de Varisie. À
    cheval sur leurs grands destructeurs, les Velashans parcourent leur
    domaine, protégeant sans merci les terres du sud et s'aventurant
    occasionnellement à Riddleport ou au-delà pour faire payer le prix
    fort pour leurs magnifiques montures.


.. world:location:: Veldraine

    Connue sous le nom de « Porte de Korvosa », Veldraine est un port
    commercial important et une position militaire clé, étant donné son
    emplacement à l'embouchure de la baie du Conquérant. En plus d'abriter
    une grande partie de la marine de Korvosan et de vastes quantités
    d'artillerie, la ville de Veldraine est également équipée d'un
    énorme treuil capable de soulever une immense chaîne du fond de la mer
    et de la tendre à travers l'embouchure étroite de la baie, bloquant
    Korvosa de l'océan et potentiellement échouer les attaquants dans la
    baie, où ils peuvent facilement être récupérés.


.. world:location:: Viperwall

    ornés de grands serpents de pierre, les toits coniques des nombreuses
    tours de ce château maussade brillent au clair de lune. Évitée par
    les habitants, la structure est souvent entourée d'une brume verdâtre
    de gaz toxique qui s'échappe régulièrement des sculptures à crocs
    dans ses murs.


.. world:location:: Wartle

    Un poste de traite délabré rempli de maraudeurs et de marchands de
    fourrures, Wartle est perché sur des échasses au-dessus de la boue des
    Mushfens.


.. world:location:: Whistledown

    Nommé pour les charmes en bois distinctifs qui pendent des avant-toits
    des maisons pour transformer le vent du soir venu du lac en mélodies
    envoûtantes, Whistledown se trouve à la pointe ouest des montagnes
    Fenwall, où le lac Syrantula redevient le Yondabakari. Bien que la
    ville abrite presque autant d'humains que de gnomes, Whistledown est
    généralement considéré comme la principale colonie de gnomes en
    Varisie, et la plupart des cottages pittoresques aux murs blancs sont
    dimensionnés en conséquence. Bien que la ville ait la réputation
    d'être une halte commerciale paisible et amicale, la plupart des
    visiteurs non gnomes trouvent la sérénade nocturne de la ville
    dérangeante d'une manière qu'ils ne peuvent pas tout à fait
    expliquer.


.. world:location:: Abbaye de Windsong

    établie par des moines savants et pacifistes en tant que forum de
    discussion interreligieuse, le vaste édifice de grès de l'abbaye de
    Windsong émerge des falaises du bord de mer dans de vastes arches et
    tours, des vitraux captant la lumière et des tunnels dans ses murs et
    ses fondations canalisant le vent en musique comme celui d'un orgue à
    tuyaux. Bien qu'à un moment donné le clergé de toutes les grandes
    religions de la Varisie, bonnes et mauvaises, se soient réunis ici pour
    résoudre les conflits et faire avancer diplomatiquement les objectifs
    de leurs diverses divinités, depuis la mort d'Aroden, plusieurs
    églises se sont retirées de l'assemblée, sans tenir compte des
    années de coopération et de recueillir des prophétie. Bien que
    l'abbaye soit un exploit architectural impressionnant en soi, l'abbesse
    masquée et ses conseillers les plus proches savent qu'elle est en fait
    construite sur les ruines d'une structure beaucoup plus
    ancienne.


.. world:location:: Wolf's Ear

    À une certaine époque, Wolf's Ear était la version lycanthrope d'une
    colonie de lépreux, où les loups-garous et autres humanoïdes
    persécutés pouvaient vivre ensemble dans une sécurité et un confort
    relatifs. Lorsque la ville a été annexée par Magnimar, cependant, le
    Lord-Maire a décidé que de telles choses étaient indécentes et a
    financé un pogrom par l'église d'Erastil destiné à « nettoyer »
    la ville. Dans les fracas sanglantes qui s'ensuivirent, les lycanthropes
    furent repoussés dans la clandestinité, où les chefs Magnimaires, non
    préparés à une résistance aussi passionnée, se contentèrent de les
    laisser rester. La position officielle de Magnimarian est que toutes les
    rumeurs de lycanthropie ne sont que cela, et ceux qui soulignent les
    habitudes inhabituelles des citadins sont rapidement contraints au
    silence.


.. world:location:: Wormwood Hall

    Envahi de vignes rampantes, ce grand manoir au cœur de Lurkwood
    s'accroupit de façon inquiétante, ses fenêtres sombres et ses
    linteaux recouverts d'étranges runes tordues. Bien qu'aucun d'entre eux
    ne se soit aventuré à l'intérieur, beaucoup pensent que Wormwood Hall
    est en quelque sorte lié aux saisons non naturelles de la
    forêt.


.. world:location:: Montagnes Wyvern

    le nom de cette chaîne dit tout, et les voyageurs ici sont invités à
    garder un œil sur les meutes itinérantes de prédateurs draconiques
    venimeux qui subsistent sur les chèvres et les moutons sauvages des
    montagnes.
