.. _module00:

##############
Avant de jouer
##############


Cadre et décor
==============

Nous jouons l'Ascension des seigneurs runiques (Rise of Runelords).

    De la ville côtière idylliquement paisible de Pointe-aux-sables
    à une ancienne ville perdue au sommet du monde,
    l'Ascension des seigneurs des Runes
    emmène un groupe d'aventuriers du 1er au 18e niveau et
    plonge dans les mystères du passé antique de Varisia.

    Il y a des millénaires, le puissant empire de Thassilon
    régnait sur la terre, dominé par des seigneurs despotiques
    qui maintenaient leur emprise en exploitant le pouvoir de la magie runique.
    Malgré qu'elle soit disparue pour toujours,
    la civilisation Thassilonienne a laissé des traces
    pas si loin sous la surface
    et l'un des seigneurs des runes prévoit un retour au pouvoir.

    Seuls les courageux aventuriers se dressent sur son chemin.

Cette aventure est de style médiéval-fantastique plutôt classique avec des
touches d'horreur à l'européenne. Elle comprend plusieurs scènes extérieures,
mais aussi une large part urbaines et sous-terraines.

J'espère que nous en fassions une partie très immersive avec d'intenses
moment de roleplay sérieux, tout en gardant l'ambiance légère et ludique!
En d'autres mots, jouons bien sans «virer fou a'ec ça».


Style de jeu
============


Structure scénaristique
-----------------------

Cette campagne est composée d'une trame principale ponctuée
de nombreuses trames secondaires facultatives.  Les événements se
déroulent en séquence et influent sur les événements futurs
(ce n'est pas un récit épisodique,
plus comme Game of Thrones que comme Les Simpsons).

La plupart des trames ne seront ni linéaires, ni «bac à sable», mais
plutôt un compromis entre les deux.

Les combats et autres scènes en temps réels vont se jouer sur une
grille de combat.

L'application des règles permet le :term:`meta-game thinking` pour
simplifier le jeu.  Par exemple, tout le monde sait combien chacun
des autres joueurs a de points de vie restant.  Les personnages
peuvent utiliser les connaissances des joueurs tant que ça sert le
récit.


Récompenses et niveaux
----------------------

Je n'attriburai pas d'expérience, mais plutôt vous indiquerai quand monter
de niveau.  Les jalons pour l'ascension de niveaux seront liés à la trame
principale.

La complétion de trames secondaires et l'assiduité ne permettront pas
d'acquérir les niveaux plus rapidement.  Ils pourraient permettre d'acquérir
plus de richesses et d'équipement, par contre.


Éthique et contenus
-------------------

Ton

L'hôte, les lieux et les installations
======================================



Mécaniques de jeu
=================

Style d'arbitrage
-----------------

Règles maison
-------------

Truquer les dés
---------------
