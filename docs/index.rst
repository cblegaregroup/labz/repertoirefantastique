#################################
Répertoire fantastique des poilus
#################################


.. toctree::
    :maxdepth: 2
    :hidden:

    aventures/index
    annexes/index
    pfsrd/index
    contribuer
    apropos


.. grid:: 2
    :margin: 4 4 0 0
    :gutter: 5

    .. grid-item-card:: :octicon:`book` Aventures
        :link: aventures/index
        :link-type: doc

        Découvrez certains éléments de nos aventures.


    .. grid-item-card:: :octicon:`note` Annexes
        :link: annexes/index
        :link-type: doc

        Explorez les dédales de nos contenus non triés.


    .. grid-item-card:: :octicon:`beaker` Systèmes de règles
        :link: pfsrd/index
        :link-type: doc

        Consultez nos références de règles en ligne avec beaucoup moins
        de publicité et de contenu que les autres SRD populaires.


    .. grid-item-card:: :octicon:`pencil` Contribuer
        :link: contribuer
        :link-type: doc

        Devenez un auteur publié mondialement (sur internet).


Indexes
=======

*   :ref:`Index Général <genindex>`
*   :ref:`Index de Golarion <world-index>`
*   :ref:`Index des règles <rule-index>`


.. _project's home: https://gitlab.com/cblegaregroup/labz/repertoirefantastique
