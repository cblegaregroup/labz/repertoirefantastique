.. _annexes:

###################
Annexes de Golarion
###################

Dans cette section se trouve des notes sur le monde qui n'ont pas (encore)
une place spécifique dans un module d'aventure.

.. toctree::
    :glob:

    personnages
    glossaire

