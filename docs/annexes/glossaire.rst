:orphan:

.. _characters:

Glossaire
=========

.. glossary::
    :sorted:

    metagaming
    meta-game thinking

        Dans les jeux de rôle sur table, on appelle metagame les actions
        qu'entreprennent les joueurs en tenant compte de paramètres que le
        personnage qu'ils jouent n'est pas censé connaître.
        Ce metagame est souvent considéré comme de la triche.
        Par exemple, si un joueur cherche à connaître les goûts du
        meneur de jeu pour prévoir ses décisions.
