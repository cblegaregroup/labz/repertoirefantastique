from __future__ import annotations

from pathlib import Path
from typing import List

import nox

@nox.session(reuse_venv=True)
def docs(session):
    session.install(
        "sphinx",
        "pydata-sphinx-theme",
        "sphinx-copybutton",
        "sphinx-design",
        "sphinx-compendia"
    )
    session.run(*_python_cmd(_sphinx_build_modulecmd("build/sphinx")))


@nox.session(reuse_venv=True)
def preview(session):
    built_index = Path("./build/sphinx/html/index.html").resolve()

    import webbrowser

    OPEN_IN_A_NEW_TAB = 2

    webbrowser.open(built_index.as_uri(), new=OPEN_IN_A_NEW_TAB)


def _sphinx_apidoc_modulecmd() -> List[str]:
    return [
        "sphinx.ext.apidoc",
        "-o",
        "docs/apidoc",
        "--force",
        "--no-toc",
        "--separate",
        "--module-first",
        "src",
    ]


def _sphinx_build_modulecmd(build_root: str) -> List[str]:
    return [
        "sphinx",
        "-b",
        "html",
        "-d",
        f"{build_root}/doctrees",
        "-E",
        "-n",
        #"-W",
        "--keep-going",
        "-T",
        "docs",
        f"{build_root}/html",
    ]


def _python_cmd(modulecmd: List[str]) -> List[str]:
    return ["python", "-m", *modulecmd]

